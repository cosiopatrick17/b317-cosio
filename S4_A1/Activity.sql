SELECT * FROM artists WHERE name LIKE '%d%';

SELECT * FROM songs WHERE length < 350;

SELECT albums.album_title, songs.song_name, songs.length
FROM albums
JOIN songs ON albums.id = songs.album_id;

SELECT artists.id, artists.name, albums.album_title, albums.date_released
FROM artists
JOIN albums ON artists.id = albums.artist_id
WHERE albums.album_title LIKE '%a%';


SELECT albums.album_title, albums.date_released, albums.artist_id
FROM albums
ORDER BY albums.album_title DESC
LIMIT 4;


SELECT albums.id album_id, albums.album_title, songs.id, songs.song_name, songs.length, songs.genre, albums.artist_id
FROM albums
JOIN songs ON albums.id = songs.album_id
ORDER BY albums.album_title DESC;

